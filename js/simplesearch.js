(function($) {

  $(document).ready(function() {
    $('.block-simplesearch').each(function() {
      var $block = $(this).find('.simplesearch-block-wrapper');
      var forms_count = $block.find('.simplesearch-form-wrapper').length;
      if (forms_count > 1) {
        // Generate form selector as radio buttons
        var $form_selector = $('<div class="simplesearch-form-selector"></div>');
        $block.find('.simplesearch-form-wrapper').each(function() {
          var title = $(this).attr('data-title');
          var name = $(this).attr('data-name');
          var $input = $('<input />')
            .attr('type', 'radio')
            .attr('id', 'simplesearch_form_selector_' + $block.attr('id') + '_' + name)
            .attr('name', 'simplesearch_form_selector_' + $block.attr('id'))
            .attr('data-form-wrapper-id', $(this).attr('id'));
          var $label = $('<label></label>')
            .attr('for', 'simplesearch_form_selector_' + $block.attr('id') + '_' + name)
            .html(title);
          var $container = $('<div></div>');
          $container.append($input).append($label);
          $form_selector.append($container);
          $(this).hide();
        });
        $block.prepend($form_selector);

        // Select the default form.
        var block_name = $block.attr('data-name');
        var default_form;
        for (form_name in Drupal.settings.simplesearch[block_name]) {
          var form = Drupal.settings.simplesearch[block_name][form_name];
          if (form.is_default == 1) {
            default_form = form.form_name;
            break;
          }
        }
        if (form_name) {
          $form_selector.find('input[data-form-wrapper-id="simplesearch_block_' + block_name + '_form_' + form_name + '_wrapper"]').attr('checked', 'checked');
        } else {
          $form_selector.find('input').first().attr('checked', 'checked');
        }

        // Change displayed form when user click on radio button.
        $form_selector.find('input').change(function() {
          var form_wrapper_id = $(this).attr('data-form-wrapper-id');
          if ($(this).is(':checked')) {
            $block.find('.simplesearch-form-wrapper').hide();
            $block.find('#' + form_wrapper_id).show();
          }
        }).change();
      }
    });
  });

})(jQuery);
