<?php
/**
 * @file
 * Admin function for parameters.
 */

/**
 * Form constructor for the parameters admin page.
 *
 * @ingroup forms
 */
function simplesearch_parameters_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $parameters = simplesearch_get_parameters();

  $info_parameters = "<span><div><b>Parameters</b></div><div class='description'>
    Add as many parameters as you want and link them to indexes.</div></span>";
  $form['info_parameters'] = array('#markup' => $info_parameters);

  $form['#tree'] = TRUE;
  // Build tabledrag for indexes.
  foreach ($parameters as $i) {
    // Format paremeters output.
    $parameter_html = $i['parameter'] . '=' . $i['value'];

    // Format indexes output.
    $parameters_indexes = simplesearch_get_parameter_indexes($i['parameter_name']);
    $indexes_string = implode(', ', array_keys($parameters_indexes));
    $key = $i['parameter_name'];
    $form[$key]['parameter_name'] = array('#markup' => check_plain($i['parameter_name']));
    $form[$key]['indexes'] = array('#markup' => check_plain($indexes_string));
    $form[$key]['parameter'] = array('#markup' => $parameter_html);

    // Operations.
    $form[$key]['edit'] = array(
      '#markup' => l(t('edit'), 'admin/structure/simplesearch/parameters/edit/' . $i['parameter_name']),
    );
    $form[$key]['delete'] = array(
      '#markup' => l(t('delete'), 'admin/structure/simplesearch/parameters/delete/' . $i['parameter_name']),
    );

  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form constructor for adding/editing index parameters.
 *
 * @param string $parameter_name
 *   Parameter name to edit.
 *
 * @see simplesearch_parameter_edit_form_submit()
 * @ingroup forms
 */
function simplesearch_parameter_edit_form($form, &$form_state, $parameter_name = NULL) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $parameters = simplesearch_get_parameters();
  $edit = isset($parameters[$parameter_name]) ? $parameters[$parameter_name] : array();

  if (!count($edit)) {
    $form['new'] = array('#type' => 'hidden', '#value' => TRUE);
  }

  $form['parameter'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameter'),
    '#description' => t('This parameter will be added to the url.'),
    '#default_value' => isset($edit['parameter']) ? $edit['parameter'] : '',
    '#size' => 40,
    '#required' => TRUE,
  );
  $form['parameter_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('parameter machine name'),
    '#default_value' => $parameter_name ? $parameter_name : '',
    '#maxlength' => 255,
    '#description' => t('A unique name for the parameter. It must only contain lowercase letters, numbers and hyphens.'),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'simplesearch_parameter_exists',
      'source' => array('parameter'),
      'label' => t('machine name'),
      'replace' => '_',
    ),
    '#disabled' => count($edit),
  );
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameter value'),
    '#description' => t('Value of the paramter. You can type "[term]" that represents the value of search textbox.'),
    '#default_value' => isset($edit['value']) ? $edit['value'] : '',
    '#size' => 30,
  );

  $indexes = simplesearch_get_indexes();
  $indexes_options = array();
  foreach ($indexes as $index_name => $index) {
    $indexes_options[$index_name] = $index['index_title'];
  }

  $default_indexes = array();
  if ($parameter_name) {
    $default_indexes = simplesearch_get_parameter_indexes($parameter_name);
  }
  if (count($indexes_options)) {
    $form['indexes_names'] = array(
      '#title' => t('Check the index(es) this parameter will be available for'),
      '#type' => 'checkboxes',
      '#default_value' => array_keys($default_indexes),
      '#options' => $indexes_options,
    );
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for simplesearch_parameter_edit_form().
 */
function simplesearch_parameter_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['new'])) {
    simplesearch_add_parameter($values);
  }
  else {
    simplesearch_edit_parameter($values);
  }
  drupal_goto('admin/structure/simplesearch/parameters');
}

/**
 * Form constructor which is a confirmation step when deleting a parameter.
 *
 * @param string $parameter_name
 *   Machine name of the parameter to delete.
 *
 * @see simplesearch_parameter_delete_confirm_form_submit()
 * @ingroup forms
 */
function simplesearch_parameter_delete_confirm_form($form, &$index_state, $parameter_name) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $form['parameter_name'] = array(
    '#type' => 'hidden',
    '#value' => $parameter_name,
  );
  $form['infos'] = array('#markup' => "Are you sure you want delete this parameter: $parameter_name");
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler.
 *
 * For simplesearch_parameter_delete_confirm_form().
 */
function simplesearch_parameter_delete_confirm_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  simplesearch_delete_parameters($values['parameter_name']);
  drupal_goto('admin/structure/simplesearch/parameters');
}
