<?php
/**
 * @file
 * Adminstration form for blocks.
 */

/**
 * Form constructor for adding/editing a block.
 *
 * @ingroup blocks
 */
function simplesearch_blocks_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  global $base_url;

  $info_blocks = "<span><div><b>Blocks</b></div><div class='description'>
    Add as many blocks as you want.</div></span>";
  $form['info_blocks'] = array('#markup' => $info_blocks);

  $blocks = simplesearch_get_blocks();

  // Build table for blocks.
  foreach ($blocks as $b) {
    // Format indexes output.
    $forms = simplesearch_get_forms($b['block_name']);
    $forms_html = '';
    foreach ($forms as $f) {
      $forms_html .= '<div>' . $f['form_title'] . ' (' . $f['form_name'] . ')</div>';
    }
    $block_conf = "";
    $key = $b['block_name'];
    $form[$key]['block_name'] = array('#markup' => check_plain($b['block_name']));
    $form[$key]['block_title'] = array('#markup' => check_plain($b['block_title']));
    $form[$key]['block_conf'] = array('#markup' => $block_conf);
    $form[$key]['forms'] = array('#markup' => $forms_html);

    // Operations.
    $form[$key]['edit'] = array(
      '#markup' => l(t('edit'), "admin/structure/simplesearch/blocks/edit/$key"),
    );
    $form[$key]['delete'] = array(
      '#markup' => l(t('delete'), "admin/structure/simplesearch/blocks/delete/$key"),
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form constructor for adding/editing a block.
 *
 * @param string $block_name
 *   Name of the block to edit.
 *
 * @see simplesearch_block_edit_form_submit()
 * @ingroup blocks
 */
function simplesearch_block_edit_form($form, &$form_state, $block_name = NULL) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $blocks = simplesearch_get_blocks();
  $edit = isset($blocks[$block_name]) ? $blocks[$block_name] : array();

  if (!count($edit)) {
    $form['new'] = array('#type' => 'hidden', '#value' => TRUE);
  }

  $form['block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($edit['block_title']) ? $edit['block_title'] : '',
    '#size' => 30,
    '#required' => TRUE,
  );
  $form['block_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Block name'),
    '#default_value' => isset($edit['block_name']) ? $edit['block_name'] : '',
    '#maxlength' => 32,
    '#description' => t('A unique name for the block. It must only contain lowercase letters, numbers and hyphens.'),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'simplesearch_block_exists',
      'source' => array('block_title'),
      'label' => t('machine name'),
      'replace' => '_',
    ),
    '#disabled' => count($edit),
  );

  $form['block_forms'] = array(
    '#type' => 'container',
    '#theme' => 'simplesearch_block_forms_table',
    '#tree' => TRUE,
  );

  $block_forms = isset($edit['block_name']) ? simplesearch_get_block_forms($edit['block_name']) : NULL;
  $forms = simplesearch_get_forms();

  uksort($forms, function ($a, $b) use($block_forms) {
    $a_weight = isset($block_forms[$a]) ? $block_forms[$a]['weight'] : 0;
    $b_weight = isset($block_forms[$b]) ? $block_forms[$b]['weight'] : 0;
    return $a_weight - $b_weight;
  });

  foreach ($forms as $form_name => $f) {
    $form['block_forms'][$form_name] = array(
      '#type' => 'fieldset',
      '#title' => $f['form_title'],
    );
    $form['block_forms'][$form_name]['form_name'] = array(
      '#type' => 'hidden',
      '#value' => $form_name,
    );
    $form['block_forms'][$form_name]['title'] = array(
      '#markup' => $f['form_title'],
    );
    $form['block_forms'][$form_name]['use'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use ?"),
      '#default_value' => isset($block_forms[$form_name]) ? 1 : 0,
    );
    $form['block_forms'][$form_name]['is_default'] = array(
      '#type' => 'checkbox',
      '#title' => t("Default ?"),
      '#default_value' => isset($block_forms[$form_name]) ? $block_forms[$form_name]['is_default'] : 0,
    );
    $form['block_forms'][$form_name]['weight'] = array(
      '#type' => 'weight',
      '#title' => t("Weight"),
      '#delta' => count($forms),
      '#default_value' => isset($block_forms[$form_name]) ? $block_forms[$form_name]['weight'] : 0,
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form validation handler for simplesearch_block_edit_form().
 */
function simplesearch_block_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $use_count = 0;
  $default_count = 0;
  foreach ($values['block_forms'] as $name => $f) {
    $use_count += $f['use'];
    $default_count += $f['is_default'];
    if ($f['is_default'] && !$f['use']) {
      form_set_error("block_forms][$name][is_default",
        t("You cannot choose a disabled form as default."));
    }
  }

  if ($use_count < 1) {
    form_set_error('', t("You must choose at least one form."));
  }

  if ($default_count < 1) {
    form_set_error('', t("You must choose one default form."));
  }
  elseif ($default_count > 1) {
    form_set_error('', t("You must choose only one default form."));
  }
}

/**
 * Form submission handler for simplesearch_block_edit_form().
 */
function simplesearch_block_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['new'])) {
    simplesearch_add_block($values);
  }
  else {
    simplesearch_edit_block($values);
  }
  drupal_goto('admin/structure/simplesearch/blocks');
}

/**
 * Form constructor which is a confirmation step when deleting a block.
 *
 * @param string $block_name
 *   Name of the block to delete.
 *
 * @see simplesearch_block_delete_confirm_form_submit()
 * @ingroup blocks
 */
function simplesearch_block_delete_confirm_form($form, &$form_state, $block_name) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $form['block_name'] = array(
    '#type' => 'hidden',
    '#value' => $block_name,
  );
  $form['infos'] = array('#markup' => "Are you sure you want delete this block: $block_name ?");
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler.
 *
 * For simplesearch_block_delete_confirm_form().
 */
function simplesearch_block_delete_confirm_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  simplesearch_delete_block($values['block_name']);
  drupal_goto('admin/structure/simplesearch/blocks');
}
