********************************************************************
                    D R U P A L    M O D U L E
********************************************************************
Name: Simple search
Author: Alex Arnaud <alex.arnaud@biblibre.com>
Drupal: 7
Maintainers:
 * Alex Arnaud (Alex Arnaud) <alex.arnaud@biblibre.com>

********************************************************************
DESCRIPTION:

This module provides a simple search block. It is different from
the default D7 search block because it allows to set where and how
to perform and redirect a search in a block.

With this module you are able to set some fields. So a user can
choose wherein he want to search (i.e 'author' field or 'title"
etc...). Once the user submitted the form in block, he is redirected
to the sepcified search form (i.e a view).

Also, you can add some additional parameters and options. Parameters
are attached to a field. Meaning they are automatically passed in the
redirect url when searching on that field. Options can be choosen or
not by the user. They are displayed as chekboxes that the user can
check or not to add a filter in the search.

********************************************************************
INSTALLATION:

forthcoming documentation.
