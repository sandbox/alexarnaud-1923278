<?php
/**
 * @file
 * Adminstration form for forms.
 */

/**
 * Form constructor for adding/editing a form.
 *
 * @ingroup forms
 */
function simplesearch_forms_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  global $base_url;

  $info_forms = "<span><div><b>Forms</b></div><div class='description'>
    Add as many forms as you want.</div></span>";
  $form['info_forms'] = array('#markup' => $info_forms);

  $forms = simplesearch_get_forms();

  // Build table for forms.
  foreach ($forms as $b) {
    // Format indexes output.
    $indexes = simplesearch_get_indexes($b['form_name']);
    $indexes_html = '';
    foreach ($indexes as $index) {
      $indexes_html .= '<div>' . $index['index_title'] . ' (' . $index['index_name'] . ')</div>';
    }
    $form_conf = "Path to the search form: <b>" . $base_url . "/" . $b['form_conf']['search_form_path'] . "</b><br />";
    $form_conf .= "Show search form link in the form: ";
    $form_conf .= $b['form_conf']['search_form_path_link'] ? "<b>Yes (Title: " . $b['form_conf']['search_form_path_title'] . ")</b>" : "<b>No</b>";
    $form_conf .= "<br />Options style: <b>" . $b['form_conf']['simplesearch_options_style'] . "</b>";
    $key = $b['form_name'];
    $form[$key]['form_name'] = array('#markup' => check_plain($b['form_name']));
    $form[$key]['form_title'] = array('#markup' => check_plain($b['form_title']));
    $form[$key]['form_conf'] = array('#markup' => $form_conf);
    $form[$key]['indexes'] = array('#markup' => $indexes_html);

    // Operations.
    $form[$key]['edit'] = array(
      '#markup' => l(t('edit'), "admin/structure/simplesearch/forms/edit/$key"),
    );
    $form[$key]['delete'] = array(
      '#markup' => l(t('delete'), "admin/structure/simplesearch/forms/delete/$key"),
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form constructor for adding/editing a form.
 *
 * @param string $form_name
 *   Name of the form to edit.
 *
 * @see simplesearch_form_edit_form_submit()
 * @ingroup forms
 */
function simplesearch_form_edit_form($form, &$form_state, $form_name = NULL) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $forms = simplesearch_get_forms();
  $edit = isset($forms[$form_name]) ? $forms[$form_name] : array();

  if (!count($edit)) {
    $form['new'] = array('#type' => 'hidden', '#value' => TRUE);
  }

  $form['form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($edit['form_title']) ? $edit['form_title'] : '',
    '#size' => 30,
    '#required' => TRUE,
  );
  $form['form_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Form name'),
    '#default_value' => isset($edit['form_name']) ? $edit['form_name'] : '',
    '#maxlength' => 32,
    '#description' => t('A unique name for the form. It must only contain lowercase letters, numbers and hyphens.'),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'simplesearch_form_exists',
      'source' => array('form_title'),
      'label' => t('machine name'),
      'replace' => '_',
    ),
    '#disabled' => count($edit),
  );
  $form['search_form_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Search form path'),
    '#description' => t('Type here the path of the search form where the will redirect. i.e "catalog/search".'),
    '#default_value' => isset($edit['form_conf']['search_form_path']) ? $edit['form_conf']['search_form_path'] : '',
    '#size' => 50,
    '#required' => TRUE,
  );
  $form['search_form_path_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show search form link in the form.'),
    '#default_value' => isset($edit['form_conf']['search_form_path_link']) ? $edit['form_conf']['search_form_path_link'] : 0,
  );
  $form['search_form_path_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Link title'),
    '#description' => t('Title that will appear in the form.'),
    '#default_value' => isset($edit['form_conf']['search_form_path_title']) ? $edit['form_conf']['search_form_path_title'] : '',
    '#size' => 30,
    '#states' => array(
      'visible' => array(
        ':input[name="search_form_path_link"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['simplesearch_options_style'] = array(
    '#type' => 'radios',
    '#title' => t('Options style'),
    '#default_value' => isset($edit['form_conf']['simplesearch_options_style']) ? $edit['form_conf']['simplesearch_options_style'] : 'checkboxes',
    '#options' => array(
      'checkboxes' => 'Checkboxes',
      'radios' => 'Radios',
      'select' => 'Select list',
    ),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for simplesearch_form_edit_form().
 */
function simplesearch_form_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['new'])) {
    simplesearch_add_form($values);
  }
  else {
    simplesearch_edit_form($values);
  }
  drupal_goto('admin/structure/simplesearch/forms');
}

/**
 * Form constructor which is a confirmation step when deleting a form.
 *
 * @param string $form_name
 *   Name of the form to delete.
 *
 * @see simplesearch_form_delete_confirm_form_submit()
 * @ingroup forms
 */
function simplesearch_form_delete_confirm_form($form, &$form_state, $form_name) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $form['form_name'] = array(
    '#type' => 'hidden',
    '#value' => $form_name,
  );
  $form['infos'] = array('#markup' => "Are you sure you want delete this form: $form_name ?");
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler.
 *
 * For simplesearch_form_delete_confirm_form().
 */
function simplesearch_form_delete_confirm_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  simplesearch_delete_form($values['form_name']);
  drupal_goto('admin/structure/simplesearch/forms');
}
