<?php

/**
 * @file
 * Displays the simple search form block.
 *
 * Available variables:
 * - form: The complete search form ready for print.
 */
?>
<div class="container-inline">
  <?php if (isset($form['search_index'])): ?>
    <?php
      print drupal_render($form['search_index']);
      unset($form['simplesearch_title']);
    ?>
  <?php else: ?>
    <?php print drupal_render($form['simplesearch_title']); ?>
  <?php endif; ?>
    <?php print drupal_render($form['search_term']); ?>
    <?php print drupal_render($form['search_submit']); ?>
</div>
<div class="simplesearch-search-options">
  <?php if (isset($form['search_options'])): ?>
    <?php print drupal_render($form['search_options']); ?>
  <?php endif; ?>
</div>
<div class="simplesearch-search-form-link">
  <?php if (isset($form['search_form_link'])): ?>
    <?php print drupal_render($form['search_form_link']); ?>
  <?php endif; ?>
</div>
<?php print drupal_render_children($form); ?>
