<?php
/**
 * @file
 * Adminstration form for indexes.
 */

/**
 * Form constructor for the indexes admin page.
 *
 * @see simplesearch_indexes_form_submit()
 * @ingroup forms
 */
function simplesearch_indexes_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $indexes = simplesearch_get_indexes();

  $info_indexes = "<span><div><b>Indexes</b></div><div class='description'>
    Add as many indexes as you want. They will appear in the dropdown on search form.</div></span>";
  $form['info_indexes'] = array('#markup' => $info_indexes);

  $form['#tree'] = TRUE;
  // Build tabledrag for indexes.
  foreach ($indexes as $i) {
    // Format paremeters output.
    $parameters = simplesearch_get_parameters($i['index_name']);
    $parameter_html = '';
    foreach ($parameters as $parameter) {
      $parameter_html .= '<div>' . $parameter['parameter'] . '=' . $parameter['value'] . '</div>';
    }

    // Format forms output.
    $indexes_form = simplesearch_get_index_form($i['index_name']);
    $forms_string = implode(', ', array_keys($indexes_form));
    $key = $i['index_name'];
    $form[$key]['index_name'] = array('#markup' => check_plain($i['index_name']));
    $form[$key]['index_title'] = array('#markup' => check_plain($i['index_title']));
    $form[$key]['forms'] = array('#markup' => check_plain($forms_string));
    $form[$key]['parameters'] = array('#markup' => $parameter_html);

    $form[$key]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $i['index_title'])),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $i['weight'],
    );

    // Operations.
    $form[$key]['edit'] = array(
      '#markup' => l(t('edit'), 'admin/structure/simplesearch/indexes/edit/' . $i['index_name']),
    );
    $form[$key]['delete'] = array(
      '#markup' => l(t('delete'), 'admin/structure/simplesearch/indexes/delete/' . $i['index_name']),
    );

  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form submission handler for simplesearch_indexes_form().
 */
function simplesearch_indexes_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  foreach (element_children($values) as $key) {
    if (isset($values[$key]['weight'])) {
      simplesearch_mod_index_weight($key, $values[$key]['weight']);
    }
  }
}

/**
 * Form constructor for adding/editing fields.
 *
 * @see simplesearch_index_edit_form_submit()
 * @ingroup forms
 */
function simplesearch_index_edit_form($form, &$form_state, $index_name = NULL) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $indexes = simplesearch_get_indexes();
  $edit = isset($indexes[$index_name]) ? $indexes[$index_name] : array();

  if (!count($edit)) {
    $form['new'] = array('#type' => 'hidden', '#value' => TRUE);
  }

  $form['index_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Index title'),
    '#description' => t('Arbitrary title of the index that will be shown in the indexes dropdown list.'),
    '#default_value' => isset($edit['index_title']) ? $edit['index_title'] : '',
    '#size' => 20,
    '#required' => TRUE,
  );
  $form['index_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('index machine name'),
    '#default_value' => isset($edit['index_name']) ? $edit['index_name'] : '',
    '#maxlength' => 32,
    '#description' => t('A unique name for the index. It must only contain lowercase letters, numbers and hyphens.'),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'simplesearch_index_exists',
      'source' => array('index_title'),
      'label' => t('machine name'),
      'replace' => '_',
    ),
    '#disabled' => count($edit),
  );
  $forms = simplesearch_get_forms();
  $form_options = array();
  foreach ($forms as $form_name => $f) {
    $form_options[$form_name] = $f['form_title'];
  }

  $default_forms = array();
  if ($index_name) {
    $default_forms = simplesearch_get_index_form($index_name);
  }
  if (count($form_options)) {
    $form['form_names'] = array(
      '#title' => t('Check the form(s) this index will be available for'),
      '#type' => 'checkboxes',
      '#default_value' => array_keys($default_forms),
      '#options' => $form_options,
    );
  }
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($edit['weight']) ? $edit['weight'] : 0,
    '#delta' => 10,
    '#description' => t('Optional. In the dropdown list, the heavier field will appear at the top.'),
  );
  if ($index_name) {
    $parameters = simplesearch_get_parameters($index_name);
    $header = array(
      t('Parameter'),
      t('Value'),
      array('data' => t('Operations'), 'colspan' => '2'),
    );
    $rows = array();
    foreach ($parameters as $p) {
      $rows[] = array(
        $p['parameter'],
        $p['value'],
        l(t('edit'), "admin/structure/simplesearch/parameters/edit/" . $p['parameter_name']),
        l(t('delete'), "admin/structure/simplesearch/parameters/delete/" . $p['parameter_name']),
      );
    }
    $form['parameters'] = array(
      '#prefix' => '<div id="simple-search-parameters">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No parameter.'),
    );
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for simplesearch_index_edit_form().
 */
function simplesearch_index_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['new'])) {
    simplesearch_add_index($values);
  }
  else {
    simplesearch_edit_index($values);
  }
  drupal_goto('admin/structure/simplesearch/indexes');
}

/**
 * Form constructor which is a confirmation step when deleting an index.
 *
 * @param string $index_name
 *   Name of the index to delete.
 *
 * @see simplesearch_index_delete_confirm_form_submit()
 * @ingroup forms
 */
function simplesearch_index_delete_confirm_form($form, &$form_state, $index_name) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $form['index_name'] = array(
    '#type' => 'hidden',
    '#value' => $index_name,
  );
  $form['infos'] = array(
    '#markup' => "Are you sure you want delete this field: " . $index_name,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler.
 *
 * For simplesearch_index_delete_confirm_form().
 */
function simplesearch_index_delete_confirm_form_submit($form, &$form_state) {
  simplesearch_delete_index($form_state['values']['index_name']);
  drupal_goto('admin/structure/simplesearch/indexes');
}
