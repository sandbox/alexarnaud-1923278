<?php
/**
 * @file
 * DB function fro Simple search module.
 */

/**
 * Get all rows in simplesearch_blocks table.
 *
 * @return array
 *   Simple search blocks.
 */
function simplesearch_get_blocks() {
  $result = db_select('simplesearch_blocks', 'b')
    ->fields('b')
    ->execute();

  $blocks = array();
  while ($row = $result->fetchAssoc()) {
    $blocks[$row['block_name']] = $row;
    $blocks[$row['block_name']]['block_conf'] = unserialize($row['block_conf']);
  }
  return $blocks;
}

/**
 * Callback that check if a block name already exists.
 *
 * @param string $block_name
 *   The name to check.
 *
 * @return bool
 *   TRUE if the name already exists.
 */
function simplesearch_block_exists($block_name) {
  $blocks = simplesearch_get_blocks();
  if (isset($blocks[$block_name])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Add a block in simplesearch_blocks table.
 *
 * @param array $values
 *   fields values.
 */
function simplesearch_add_block($values) {
  $block_conf = serialize($values);
  $result = db_insert('simplesearch_blocks')
    ->fields(array(
      'block_name' => $values['block_name'],
      'block_title' => $values['block_title'],
      'block_conf' => $block_conf,
    ))
    ->execute();

  if (isset($values['block_forms'])) {
    simplesearch_blocks_forms_edit($values['block_name'], $values['block_forms']);
  }
}

/**
 * Edit a block in simplesearch_blocks table.
 *
 * @param array $values
 *   fields values.
 */
function simplesearch_edit_block($values) {
  $block_conf = serialize($values);
  $updated = db_update('simplesearch_blocks')
    ->fields(array(
      'block_title' => $values['block_title'],
      'block_conf' => $block_conf,
    ))
    ->condition('block_name', $values['block_name'])
    ->execute();

  if (isset($values['block_forms'])) {
    simplesearch_blocks_forms_edit($values['block_name'], $values['block_forms']);
  }
}

/**
 * Delete a block.
 *
 * @param string $block_name
 *   Machine name of the block to delete.
 */
function simplesearch_delete_block($block_name) {
  $num_deleted = db_delete('simplesearch_blocks')
    ->condition('block_name', $block_name)
    ->execute();
  $num_deleted = db_delete('simplesearch_blocks_forms')
    ->condition('block_name', $block_name)
    ->execute();
}


/**
 * Get all rows in simplesearch_forms table.
 *
 * @return array
 *   Simple search forms.
 */
function simplesearch_get_forms($block_name = NULL) {
  $query = db_select('simplesearch_forms', 'f');
  $query->leftjoin('simplesearch_blocks_forms', 'bf', 'f.form_name = bf.form_name');
  $query->fields('f');

  if (isset($block_name)) {
    $query->condition('block_name', $block_name);
  }

  $result = $query->execute();

  $forms = array();
  while ($row = $result->fetchAssoc()) {
    $forms[$row['form_name']] = $row;
    $forms[$row['form_name']]['form_conf'] = unserialize($row['form_conf']);
  }
  return $forms;
}

/**
 * Callback that check if a form name already exists.
 *
 * @param string $form_name
 *   The name to check.
 *
 * @return bool
 *   TRUE if the name already exists.
 */
function simplesearch_form_exists($form_name) {
  $forms = simplesearch_get_forms();
  if (isset($forms[$form_name])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Add a form in simplesearch_forms table.
 *
 * @param array $values
 *   fields values.
 */
function simplesearch_add_form($values) {
  $form_conf = serialize($values);
  $result = db_insert('simplesearch_forms')
    ->fields(array(
      'form_name' => $values['form_name'],
      'form_title' => $values['form_title'],
      'form_conf' => $form_conf,
    ))
    ->execute();
}

/**
 * Edit a form in simplesearch_forms table.
 *
 * @param array $values
 *   fields values.
 */
function simplesearch_edit_form($values) {
  $form_conf = serialize($values);
  $updated = db_update('simplesearch_forms')
    ->fields(array(
      'form_title' => $values['form_title'],
      'form_conf' => $form_conf,
    ))
    ->condition('form_name', $values['form_name'])
    ->execute();
}

/**
 * Delete a form.
 *
 * @param string $form_name
 *   Machine name of the form to delete.
 */
function simplesearch_delete_form($form_name) {
  $num_deleted = db_delete('simplesearch_forms')
    ->condition('form_name', $form_name)
    ->execute();
  $num_deleted = db_delete('simplesearch_indexes_forms')
    ->condition('form_name', $form_name)
    ->execute();
}

/**
 * Get existing form-block relationship for a given block.
 *
 * @param string $block_name
 *   Machine name of the block.
 *
 * @return array
 *   Array containing forms.
 */
function simplesearch_get_block_forms($block_name) {
  $query = db_select('simplesearch_blocks_forms', 'bf');
  $query->leftjoin('simplesearch_forms', 'f', 'bf.form_name = f.form_name');
  $result = $query->fields('bf')
    ->fields('f')
    ->condition('block_name', $block_name)
    ->orderBy('weight', 'ASC')
    ->execute();

  $block_forms = array();
  while ($row = $result->fetchAssoc()) {
    $block_forms[$row['form_name']] = $row;
  }
  return $block_forms;
}

/**
 * Set forms for a given block.
 *
 * @param string $block_name
 *   Related block name.
 *
 * @param array $forms
 *   Array of associative array which contain keys: form_name, use, is_default
 *   and weight.
 */
function simplesearch_blocks_forms_edit($block_name, $forms) {
  db_delete('simplesearch_blocks_forms')
    ->condition('block_name', $block_name)
    ->execute();

  foreach ($forms as $form) {
    if ($form['use']) {
      db_insert('simplesearch_blocks_forms')
        ->fields(array(
          'block_name' => $block_name,
          'form_name' => $form['form_name'],
          'is_default' => (int) $form['is_default'],
          'weight' => (int) $form['weight'],
        ))
        ->execute();
    }
  }
}

/**
 * Add an index in simplesearch_indexes table.
 *
 * @param array $values
 *   fields values.
 */
function simplesearch_add_index($values) {
  $result = db_insert('simplesearch_indexes')
    ->fields(array(
      'index_name' => $values['index_name'],
      'index_title' => $values['index_title'],
      'weight' => $values['weight'],
    ))
    ->execute();

  if (isset($values['form_names'])) {
    simplesearch_index_form_edit($values['index_name'], $values['form_names']);
  }
}

/**
 * Edit an index in simplesearch_indexes table.
 *
 * @param array $values
 *   fields values.
 */
function simplesearch_edit_index($values) {
  $updated = db_update('simplesearch_indexes')
    ->fields(array(
      'index_title' => $values['index_title'],
      'weight' => $values['weight'],
    ))
    ->condition('index_name', $values['index_name'])
    ->execute();

  if (isset($values['form_names'])) {
    simplesearch_index_form_edit($values['index_name'], $values['form_names']);
  }
}

/**
 * Get all rows in simplesearch_indexes table.
 *
 * @return array
 *   Indexes available to search in.
 */
function simplesearch_get_indexes($form_name = NULL) {
  $query = db_select('simplesearch_indexes', 'i');
  $query->leftjoin('simplesearch_indexes_forms', 'f', 'i.index_name = f.index_name');
  $query->fields('i')->orderBy('weight', 'ASC');

  if ($form_name) {
    $query->condition('form_name', $form_name);
  }

  $indexes = array();
  $result = $query->execute();

  while ($row = $result->fetchAssoc()) {
    $indexes[$row['index_name']] = $row;
  }
  return $indexes;
}

/**
 * Get existing index-form relationship for a given index name.
 *
 * @param string $index_name
 *   Machine name of the index.
 *
 * @return array
 *   all indexex in an array keyed by form name.
 */
function simplesearch_get_index_form($index_name) {
  $result = db_select('simplesearch_indexes_forms', 'ifo')
    ->fields('ifo')
    ->condition('index_name', $index_name)
    ->execute();

  $indexes_form = array();
  while ($row = $result->fetchAssoc()) {
    $indexes_form[$row['form_name']] = $row['index_name'];
  }
  return $indexes_form;
}

/**
 * Add/Remove forms for a given index.
 *
 * @param string $index_name
 *   Related index name.
 *
 * @param array $form_names
 *   All forms linked to the index.
 */
function simplesearch_index_form_edit($index_name, $form_names) {
  $query = db_delete('simplesearch_indexes_forms')
    ->condition('index_name', $index_name, '=');
  $query->execute();
  foreach ($form_names as $form_name => $checked) {
    if (!$checked) {
      continue;
    }
    // Add a form - index.
    $result = db_insert('simplesearch_indexes_forms')
      ->fields(array(
        'index_name' => $index_name,
        'form_name' => $form_name,
      ))
      ->execute();
  }
}

/**
 * Change weight of a given index.
 *
 * @param string $index_name
 *   Index machine name to change the weight.
 *
 * @param int $weight
 *   New weight of the index.
 */
function simplesearch_mod_index_weight($index_name, $weight) {
  $updated = db_update('simplesearch_indexes')
    ->fields(array('weight' => $weight))
    ->condition('index_name', $index_name)
    ->execute();
}

/**
 * Delete an index.
 *
 * @param string $index_name
 *   Index machine name to delete.
 */
function simplesearch_delete_index($index_name) {
  $num_deleted = db_delete('simplesearch_indexes')
    ->condition('index_name', $index_name)
    ->execute();

  simplesearch_delete_parameters($index_name);

  // Delete form-index relation.
  $num_deleted = db_delete('simplesearch_indexes_forms')
    ->condition('index_name', $index_name)
    ->execute();

  // Delete parameter-index relation.
  $num_deleted = db_delete('simplesearch_indexes_parameters')
    ->condition('index_name', $index_name)
    ->execute();
}

/**
 * Callback that check if an index name already exists.
 *
 * @param string $index_name
 *   The name to check.
 *
 * @return bool
 *   TRUE if the name already exists.
 */
function simplesearch_index_exists($index_name) {
  $indexes = simplesearch_get_indexes();
  if (isset($indexes[$index_name])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Get all rows in simplesearch_index_parameters table.
 *
 * Get rows for a given field name.
 *
 * @return array
 *   Fields parameters.
 */
function simplesearch_get_parameters($index_name = NULL) {
  $query = db_select('simplesearch_parameters', 'p');
  $query->leftjoin('simplesearch_indexes_parameters', 'i', 'p.parameter_name = i.parameter_name');
  $query->fields('p')->execute();

  if ($index_name) {
    $query->condition('index_name', $index_name);
  }

  $parameters = array();
  $result = $query->execute();

  while ($row = $result->fetchAssoc()) {
    $parameters[$row['parameter_name']] = $row;
  }
  return $parameters;
}

/**
 * Get index-parameter relationship.
 *
 * @param string $parameter_name
 *   Machine name of the parameter.
 *
 * @return array
 *   All parameters in an array keyed by index name.
 */
function simplesearch_get_parameter_indexes($parameter_name) {
  $result = db_select('simplesearch_indexes_parameters', 'ip')
    ->fields('ip')
    ->condition('parameter_name', $parameter_name)
    ->execute();

  $parameter_indexes = array();
  while ($row = $result->fetchAssoc()) {
    $parameter_indexes[$row['index_name']] = $row['parameter_name'];
  }
  return $parameter_indexes;
}

/**
 * Add/remove indexes for a given parameter.
 *
 * @param string $parameter_name
 *   Machine name of the parameter.
 *
 * @param array $indexes_names
 *   All indexes linked to the parameter.
 */
function simplesearch_parameters_indexes_edit($parameter_name, $indexes_names) {
  $result = db_delete('simplesearch_indexes_parameters')
    ->condition('parameter_name', $parameter_name)
    ->execute();
  foreach ($indexes_names as $index_name => $checked) {
    if (!$checked) {
      continue;
    }
    // Add a form - index.
    $result = db_insert('simplesearch_indexes_parameters')
      ->fields(array(
        'parameter_name' => $parameter_name,
        'index_name' => $index_name,
      ))
      ->execute();
  }
}

/**
 * Add a parameter in simplesearch_parameters table.
 *
 * @param array $values
 *   Parameter values.
 */
function simplesearch_add_parameter($values) {
  $result = db_insert('simplesearch_parameters')
    ->fields(array(
      'parameter_name' => $values['parameter_name'],
      'parameter' => $values['parameter'],
      'value' => $values['value'],
    ))
    ->execute();

  if (isset($values['indexes_names'])) {
    simplesearch_parameters_indexes_edit($values['parameter_name'], $values['indexes_names']);
  }
}

/**
 * Edit a parameter in simplesearch_parameters table.
 *
 * @param array $values
 *   Parameter values.
 */
function simplesearch_edit_parameter($values) {
  $mid = db_update('simplesearch_parameters')
    ->fields(array(
      'parameter' => $values['parameter'],
      'value' => $values['value'],
    ))
    ->condition('parameter_name', $values['parameter_name'])
    ->execute();

  if (isset($values['indexes_names'])) {
    simplesearch_parameters_indexes_edit($values['parameter_name'], $values['indexes_names']);
  }
}

/**
 * Delete an index parameter.
 *
 * @param string $parameter_name
 *   Parameter name to delete.
 */
function simplesearch_delete_parameters($parameter_name) {
  $result = db_delete('simplesearch_parameters')
    ->condition('parameter_name', $parameter_name)
    ->execute();

  // Delete parameter-index relation.
  $num_deleted = db_delete('simplesearch_indexes_parameters')
    ->condition('parameter_name', $parameter_name)
    ->execute();
}

/**
 * Callback that check if a paramter name already exists.
 *
 * @param string $parameter_name
 *   The paramter name to check.
 *
 * @return bool
 *   TRUE if the name already exists.
 */
function simplesearch_parameter_exists($parameter_name) {
  $result = db_select('simplesearch_parameters', 'a')
    ->fields('a')
    ->execute();

  $parameters = array();

  while ($row = $result->fetchAssoc()) {
    $parameters[$row['parameter_name']] = $row;
  }
  if (isset($parameters[$parameter_name])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Add an option for the search form.
 *
 * @param array $values
 *   Options values.
 */
function simplesearch_add_option($values) {
  $result = db_insert('simplesearch_options')
    ->fields(array(
      'option_name' => $values['option_name'],
      'option_title' => $values['option_title'],
      'parameter' => $values['parameter'],
      'value' => $values['value'],
      'weight' => $values['weight'],
    ))
    ->execute();

  if (isset($values['form_names'])) {
    simplesearch_option_form_edit($values['option_name'], $values['form_names']);
  }
}

/**
 * Edit an option for the search form.
 *
 * @param array $values
 *   Options values.
 */
function simplesearch_edit_option($values) {
  $mid = db_update('simplesearch_options')
    ->fields(array(
      'option_title' => $values['option_title'],
      'parameter' => $values['parameter'],
      'value' => $values['value'],
      'weight' => $values['weight'],
    ))
    ->condition('option_name', $values['option_name'])
    ->execute();

  if (isset($values['form_names'])) {
    simplesearch_option_form_edit($values['option_name'], $values['form_names']);
  }
}

/**
 * Get all options for search form.
 *
 * @return array
 *   Options that will be shown as checkboxes.
 */
function simplesearch_get_options() {
  $select = db_select('simplesearch_options', 'a')
    ->fields('a')
    ->orderBy('weight', 'ASC');

  $checkboxes = array();
  $result = $select->execute();

  while ($row = $result->fetchAssoc()) {
    $checkboxes[$row['option_name']] = $row;
  }
  return $checkboxes;
}

/**
 * Change weight of a given option (checkbox).
 *
 * @param string $name
 *   Machine name of the option to change the weight.
 *
 * @param int $weight
 *   New weight of the option.
 */
function simplesearch_mod_options_weight($name, $weight) {
  $mid = db_update('simplesearch_options')
    ->fields(array('weight' => $weight))
    ->condition('machine_name', $name, '=')
    ->execute();
}

/**
 * Delete a search form option.
 *
 * @param string $name
 *   Machine name of the option to delete.
 */
function simplesearch_delete_options($name) {
  $num_deleted = db_delete('simplesearch_options')
    ->condition('option_name', $name, '=')
    ->execute();

  // Delete parameter-index relation.
  $num_deleted = db_delete('simplesearch_options_forms')
    ->condition('option_name', $name)
    ->execute();
}

/**
 * Callback that check if an option name already exists.
 *
 * @param string $option_name
 *   The name to check.
 *
 * @return bool
 *   TRUE if the name already exists.
 */
function simplesearch_option_exists($option_name) {
  $options = simplesearch_get_options();
  if (isset($options[$option_name])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Get forms for a given option.
 *
 * @param string $option_name
 *   Name of the option
 *
 * @return array
 *   Array keyed with form name.
 */
function simplesearch_get_option_form($option_name) {
  $result = db_select('simplesearch_options_forms', 'of')
    ->fields('of')
    ->condition('option_name', $option_name)
    ->execute();

  $options_form = array();
  while ($row = $result->fetchAssoc()) {
    $options_form[$row['form_name']] = $row['option_name'];
  }
  return $options_form;
}

/**
 * Edit option for forms.
 *
 * @param string $option_name
 *   Name of the option for adding/removing forms
 *
 * @param array $form_names
 *   List of form names for this option.
 */
function simplesearch_option_form_edit($option_name, $form_names) {
  $options_form = simplesearch_get_option_form($option_name);
  $num_deleted = db_delete('simplesearch_options_forms')
    ->condition('option_name', $option_name)
    ->execute();
  foreach ($form_names as $form_name => $checked) {
    if (!$checked) {
      continue;
    }
    // Add a form - option.
    $result = db_insert('simplesearch_options_forms')
      ->fields(array(
        'option_name' => $option_name,
        'form_name' => $form_name,
      ))
      ->execute();
  }
}

/**
 * Retieves options for a given form.
 *
 * @param string $form_name
 *   The form name.
 *
 * @return array
 *   All form options.
 */
function simplesearch_get_option_for_form($form_name) {
  $options = simplesearch_get_options();
  $result = db_select('simplesearch_options_forms', 'of')
    ->fields('of')
    ->condition('form_name', $form_name)
    ->execute();

  $return = array();
  while ($row = $result->fetchAssoc()) {
    $return[$row['option_name']] = $options[$row['option_name']];
  }
  return $return;
}
