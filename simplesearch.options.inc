<?php
/**
 * @file
 * Adminstration form for options.
 */

/**
 * Form constructor for options admin.
 *
 * @ingroup forms
 */
function simplesearch_options_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");

  $info_options = "<span><div><b>Options (checkboxes or radios)</b></div><div class='description'>
    Checkboxes allow users to add search filters.</div></span>";
  $form['info_options'] = array('#markup' => $info_options);

  $options = simplesearch_get_options();

  // Build tabledrag for checkboxes (options).
  foreach ($options as $o) {
    $key = $o['option_name'];
    // Format forms output.
    $options_form = simplesearch_get_option_form($key);
    $forms_string = implode(', ', array_keys($options_form));

    $form[$key]['option_name'] = array('#markup' => check_plain($o['option_name']));
    $form[$key]['option_title'] = array('#markup' => check_plain($o['option_title']));
    $form[$key]['forms'] = array('#markup' => check_plain($forms_string));
    $form[$key]['parameter'] = array('#markup' => check_plain('&' . $o['parameter'] . '=' . $o['value']));

    // Operations.
    $form[$key]['edit'] = array(
      '#markup' => l(t('edit'), "admin/structure/simplesearch/options/edit/$key"),
    );
    $form[$key]['delete'] = array(
      '#markup' => l(t('delete'), "admin/structure/simplesearch/options/delete/$key"),
    );

    $form[$key]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $o['option_title'])),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $o['weight'],
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Form constructor for adding/editing an option.
 *
 * @param string $option_name
 *   Name of the option to edit.
 *
 * @see simplesearch_option_edit_form_submit()
 * @ingroup forms
 */
function simplesearch_option_edit_form($form, &$form_state, $option_name = NULL) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $options = simplesearch_get_options();
  $edit = isset($options[$option_name]) ? $options[$option_name] : array();

  if (!count($edit)) {
    $form['new'] = array('#type' => 'hidden', '#value' => TRUE);
  }

  $form['option_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Option title'),
    '#default_value' => isset($edit['option_title']) ? $edit['option_title'] : '',
    '#size' => 30,
    '#required' => TRUE,
  );
  $form['option_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Option name'),
    '#default_value' => isset($edit['option_name']) ? $edit['option_name'] : '',
    '#maxlength' => 32,
    '#description' => t('A unique name for the option. It must only contain lowercase letters, numbers and hyphens.'),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'simplesearch_option_exists',
      'source' => array('option_title'),
      'label' => t('machine name'),
      'replace' => '_',
    ),
    '#disabled' => count($edit),
  );
  $forms = simplesearch_get_forms();
  $form_options = array();
  foreach ($forms as $form_name => $f) {
    $form_options[$form_name] = $f['form_title'];
  }

  $default_forms = array();
  if ($option_name) {
    $default_forms = simplesearch_get_option_form($option_name);
  }
  if (count($form_options)) {
    $form['form_names'] = array(
      '#title' => t('Check the form(s) this index will be available for'),
      '#type' => 'checkboxes',
      '#default_value' => array_keys($default_forms),
      '#options' => $form_options,
    );
  }

  $form['parameter'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameter'),
    '#default_value' => isset($edit['parameter']) ? $edit['parameter'] : '',
    '#size' => 40,
    '#required' => TRUE,
  );
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => isset($edit['value']) ? $edit['value'] : '',
    '#size' => 40,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($edit['weight']) ? $edit['weight'] : 0,
    '#delta' => 10,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for simplesearch_option_edit_form().
 */
function simplesearch_option_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['new'])) {
    simplesearch_add_option($values);
  }
  else {
    simplesearch_edit_option($values);
  }
  drupal_goto('admin/structure/simplesearch/options');
}

/**
 * Form constructor which is a confirmation step when deleting an option.
 *
 * @param string $option_name
 *   Name of the option to delete.
 *
 * @see simplesearch_option_delete_confirm_form_submit()
 * @ingroup forms
 */
function simplesearch_option_delete_confirm_form($form, &$form_state, $option_name) {
  form_load_include($form_state, 'inc', 'simplesearch', "simplesearch.db");
  $form['option_name'] = array(
    '#type' => 'hidden',
    '#value' => $option_name,
  );
  $form['infos'] = array('#markup' => "Are you sure you want delete this checkbox: $option_name ?");
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler.
 *
 * For simplesearch_option_delete_confirm_form().
 */
function simplesearch_option_delete_confirm_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  simplesearch_delete_options($values['option_name']);
  drupal_goto('admin/structure/simplesearch/options');
}
