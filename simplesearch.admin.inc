<?php
/**
 * @file
 * Configuration page for simple search module.
 */

/**
 * Form constructor for the search block admin page.
 *
 * @see simplesearch_admin_form_submit()
 * @ingroup forms
 */
function simplesearch_admin_form($form, &$form_state) {
  $html = '<h2>' . t('Simple search settings') . '</h2>';
  $html .= '<div class="description">';
  $html .= t('Simple search allows you to create several forms than can be configured to redirect a search to a full search form.');
  $html .= '</div><br />';

  $html .= l(t('create a block'), 'admin/structure/simplesearch/blocks/add');
  $html .= '<div class="description">';
  $html .= t('You create as many blocks as you want. A block should contain at least one form. Once created, they appear in Admin > Structure > Blocks.');
  $html .= '</div><br />';

  $html .= l(t('create a form'), 'admin/structure/simplesearch/forms/add');
  $html .= '<div class="description">';
  $html .= t('You create as many forms as you want. A form can be attached to any number of blocks.');
  $html .= '</div><br />';

  $html .= l(t('create an index'), 'admin/structure/simplesearch/indexes/add');
  $html .= '<div class="description">';
  $html .= t('Create indexes and link it to some forms. Indexes are kind of fields in which searching data. They appear in a dropdown list on the form.');
  $html .= '</div><br />';

  $html .= l(t('create parameters'), 'admin/structure/simplesearch/parameters/add');
  $html .= '<div class="description">';
  $html .= t('They must be link to an index. When search for an index, all linked parameters are passed in the url');
  $html .= '</div><br />';

  $html .= l(t('create an option'), 'admin/structure/simplesearch/options/add');
  $html .= '<div class="description">';
  $html .= t('Options are additionnal filters that are display like checkboxes or radios in the form.');
  $html .= '</div><br />';

  $form['global'] = array('#markup' => $html);
  return $form;
}
