<?php
/**
 * @file
 * Theming functions for simple search module
 */

/**
 * Returns HTML for the blocks admin page.
 *
 * @param array $variables
 *   An associative array containing block elements.
 *
 * @ingroup themeable
 */
function theme_simplesearch_blocks_form($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['block_name'])) {
      $b = &$form[$key];
      $row = array();
      $row[] = drupal_render($b['block_name']);

      $row[] = drupal_render($b['block_title']);
      $row[] = drupal_render($b['block_conf']);
      $row[] = drupal_render($b['forms']);
      $row[] = drupal_render($b['edit']);
      $row[] = drupal_render($b['delete']);

      $rows[] = array('data' => $row);
    }
  }
  $header = array(t('Machine name'));
  $header[] = t('Title');
  $header[] = t('Configuration');
  $header[] = t('Forms');
  $header[] = array('data' => t('Operations'), 'colspan' => '2');

  $output = drupal_render($form['info_blocks']);
  $output .= theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('No blocks available.'),
      'attributes' => array('id' => 'search-form-blocks'),
    )
  );
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Returns HTML for the indexes admin page.
 *
 * @param array $variables
 *   An associative array containing form elements.
 *
 * @ingroup themeable
 */
function theme_simplesearch_indexes_form($variables) {
  $form = $variables['form'];

  // Table drag for indexes.
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['index_name'])) {
      $p = &$form[$key];
      $row = array();

      $row[] = drupal_render($p['index_name']);

      // Render weight.
      if (isset($p['weight'])) {
        $p['weight']['#attributes']['class'] = array('search-form-indexes-weight');
        $row[] = drupal_render($p['weight']);
      }

      // Render attributes.
      $row[] = drupal_render($p['index_title']);
      $row[] = drupal_render($p['forms']);
      $row[] = drupal_render($p['parameters']);
      $row[] = drupal_render($p['edit']);
      $row[] = drupal_render($p['delete']);

      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }

  $header = array();
  $header[] = t('Machine name');
  if (isset($p['weight'])) {
    $header[] = t('Weight');
  }
  $header[] = t('Title');
  $header[] = t('Blocks');
  $header[] = t('Parameters');
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  drupal_add_tabledrag('search-form-indexes', 'order', 'sibling', 'search-form-indexes-weight');

  $output = drupal_render($form['info_indexes']);
  $output .= theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('No index available.'),
      'attributes' => array('id' => 'search-form-indexes'),
    )
  );
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Returns HTML for the options admin page.
 *
 * @param array $variables
 *   An associative array containing form elements.
 *
 * @ingroup themeable
 */
function theme_simplesearch_options_form($variables) {
  $form = $variables['form'];

  // Table drag for option.
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['option_name'])) {
      $o = &$form[$key];
      $row = array();
      $row[] = drupal_render($o['option_name']);

      // Render weight.
      if (isset($o['weight'])) {
        $o['weight']['#attributes']['class'] = array('search-form-options-weight');
        $row[] = drupal_render($o['weight']);
      }

      $row[] = drupal_render($o['option_title']);
      $row[] = drupal_render($o['forms']);
      $row[] = drupal_render($o['parameter']);
      $row[] = drupal_render($o['edit']);
      $row[] = drupal_render($o['delete']);

      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }
  $header = array();
  $header[] = t('Option name');
  if (isset($o['weight'])) {
    $header[] = t('Weight');
  }
  $header[] = t('Title');
  $header[] = t('Blocks');
  $header[] = t('Filter');
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  drupal_add_tabledrag('search-form-options', 'order', 'sibling', 'search-form-options-weight');

  $output = drupal_render($form['info_options']);
  $output .= theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('No option available.'),
      'attributes' => array('id' => 'search-form-options'),
    )
  );
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Returns HTML for the forms admin page.
 *
 * @param array $variables
 *   An associative array containing form elements.
 *
 * @ingroup themeable
 */
function theme_simplesearch_forms_form($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['form_name'])) {
      $b = &$form[$key];
      $row = array();
      $row[] = drupal_render($b['form_name']);

      $row[] = drupal_render($b['form_title']);
      $row[] = drupal_render($b['form_conf']);
      $row[] = drupal_render($b['indexes']);
      $row[] = drupal_render($b['edit']);
      $row[] = drupal_render($b['delete']);

      $rows[] = array('data' => $row);
    }
  }
  $header = array(t('Machine name'));
  $header[] = t('Title');
  $header[] = t('Configuration');
  $header[] = t('Indexes');
  $header[] = array('data' => t('Operations'), 'colspan' => '2');

  $output = drupal_render($form['info_forms']);
  $output .= theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('No option available.'),
      'attributes' => array('id' => 'search-form-forms'),
    )
  );
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Returns HTML for the parameters admin page.
 *
 * @param array $variables
 *   An associative array containing form elements.
 *
 * @ingroup themeable
 */
function theme_simplesearch_parameters_form($variables) {
  $form = $variables['form'];

  // Table drag for indexes.
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['parameter_name'])) {
      $p = &$form[$key];
      $row = array();

      // Render attributes.
      $row[] = drupal_render($p['parameter_name']);
      $row[] = drupal_render($p['indexes']);
      $row[] = drupal_render($p['parameter']);
      $row[] = drupal_render($p['edit']);
      $row[] = drupal_render($p['delete']);

      $rows[] = array('data' => $row);
    }
  }

  $header = array();
  $header[] = t('Machine name');
  $header[] = t('Indexes');
  $header[] = t('Parameter');
  $header[] = array('data' => t('Operations'), 'colspan' => '2');

  $output = drupal_render($form['info_parameters']);
  $output .= theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('No parameter available.'),
      'attributes' => array('id' => 'search-form-parameters'),
    )
  );
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Theme simplesearch_block_forms_table.
 */
function theme_simplesearch_block_forms_table($variables) {
  $element = $variables['element'];

  $header = array(
    t("Title"),
    t("Use"),
    t("Default"),
    t("Weight"),
  );
  $rows = array();

  foreach (element_children($element) as $form_name) {
    $element[$form_name]['weight']['#attributes']['class'] = array('simplesearch-form-weight');

    unset($element[$form_name]['use']['#title']);
    unset($element[$form_name]['is_default']['#title']);
    unset($element[$form_name]['weight']['#title']);

    $rows[$form_name] = array(
      'data' => array(
        drupal_render($element[$form_name]['title']),
        drupal_render($element[$form_name]['use']),
        drupal_render($element[$form_name]['is_default']),
        drupal_render($element[$form_name]['weight']),
      ),
      'class' => array('draggable'),
    );
  }

  $table_id = 'simplesearch-block-forms-table';

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'simplesearch-form-weight');

  return $output;
}
